
import pandas as pd 
import numpy as np 
import math 
import pickle 

price_data   = pd.read_hdf('intrinio_hdf','price_data')
ticker_frame = pd.read_hdf('intrinio_hdf','ticker_frame')
id_frame     = pd.read_hdf('intrinio_hdf','id_frame')

#print (price_data)
#print (ticker_frame)
#print (id_frame)
#print ( ticker_frame.columns ) # get all the ids per a stock and then do something given a stock and a given location 

sym_to_ids = {}

ticker_frame_rows =  ticker_frame.index # contains a row called None which has been omitted here 

for row in ticker_frame_rows:

		for key,val in enumerate(ticker_frame.loc[row]):
			
			#print (row , val)

			try:

				if (type(val) == str and row!=None):

					if row in sym_to_ids:
						sym_to_ids[row].append(val)
					else:
						sym_to_ids[row] = []
						sym_to_ids[row].append(val)


				elif (math.isnan(val)):
					#print ("the value is nan")
					pass

			except:
				pass 

		try:
			if (len(sym_to_ids[row]) > 2):
				print (row , len(sym_to_ids[row]))
		
		except:
			pass 
			
		
print (sym_to_ids)
print (sym_to_ids['SRC'])

pickle.dump( sym_to_ids, open( "sym_to_ids.p", "wb" ) )


