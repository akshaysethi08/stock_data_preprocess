import pickle 
import numpy as np 
import os 
import sklearn 
from sklearn.ensemble import IsolationForest
from sklearn.neighbors import LocalOutlierFactor
from sklearn.svm import OneClassSVM

nd = pickle.load(open('nasdaq.p','rb'))
ny = pickle.load(open('nyse.p','rb'))


for company in nd:

	print (company)

	prices = nd[company][0]
	dates = prices.index.values

	#check for first
	year = (np.mean(prices[:200]))
	if (prices[0] > 2*year or prices[0] < year/2):
		prices[0] = year
		print ("flagged first")
		print (company)

	for i in range(1,prices.shape[0]):

		if (prices[i] > 2*prices[i-1] or prices[i] < prices[i-1]/2):
			print ("flagged")
			print (company)
			print (prices[i], prices[i-1], dates[i])
			
			print (prices[i-10:i+10])

			prices[i] = prices[i-1]


	nd[company][0] = prices

	print ("rerunning")

	prices = nd[company][0]

	#check for first
	year = (np.mean(prices[:200]))
	if (prices[0] > 10*year or prices[0] < year/10):
		prices[0] = year

	for i in range(1,prices.shape[0]):

		if (prices[i] > 10*prices[i-1] or prices[i] < prices[i-1]/10):
			print ("flagged")
			print (prices[i], prices[i-1])
			print (company)

			prices[i] = prices[i-1]







