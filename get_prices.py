import pandas as pd 
import numpy as np 
import math 
import pickle 
import time 

price_data   = pd.read_hdf('intrinio_hdf','price_data')
sym_to_ids   = pickle.load(open('sym_to_ids.p','rb'))

try:  # sanity check 
	print (sym_to_ids[None])
except:
	print ("no None located")

price_dict = {}

for sym in sym_to_ids:

	for ids in (sym_to_ids[sym]):
		
		df_new = price_data[ids].dropna()
		if (df_new.shape[0] > 0):
			print (sym , ids , df_new)

			if sym in price_dict:
				price_dict[sym].append(df_new)
			
			else:
				price_dict[sym] = []
				price_dict[sym].append(df_new)


pickle.dump(price_dict,open('prices_syms.p','wb'))



		


