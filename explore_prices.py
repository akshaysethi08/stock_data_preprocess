import pickle 
import pandas as pd 
import numpy as np 
import time

prices = pickle.load(open('prices_syms.p','rb'))

print (len(prices.keys()))

nasdaq = pd.read_csv('nasdaq.csv') 
nyse   = pd.read_csv('nyse.csv')

syms_nasdaq =  (nasdaq['Symbol'])
syms_nyse   =  (nyse['Symbol'])

# checking by the index

stocks_nasdaq = set()
stocks_nyse = set()

for sym in syms_nasdaq:
	stocks_nasdaq.add(sym)

for sym in syms_nyse:
	stocks_nyse.add(sym)

counter  = 0 
counter_two = 0

nyse_prices = {}
nyse_prices_two = {}

for sym in prices:
	if sym in stocks_nyse:

		if (len(prices[sym]) > 1):
			print (sym , len(prices[sym]))
			counter_two = counter_two + 1 
			nyse_prices_two[sym] = prices[sym]
		else:

			nyse_prices[sym] = prices[sym]
		
		counter = counter + 1 

print (counter)
print (counter_two)
print (len(stocks_nyse))

counter  = 0 
counter_two = 0

nasdaq_prices = {}
nasdaq_prices_two = {}

for sym in prices:

	if sym in stocks_nasdaq:

		if (len(prices[sym]) > 1):
			
			print (sym , len(prices[sym]))
			counter_two = counter_two + 1 
			nasdaq_prices_two[sym] = prices[sym]
		
		else:

			nasdaq_prices[sym] = prices[sym]
		
		counter = counter + 1 

print (counter)
print (counter_two)
print (len(stocks_nasdaq))

pickle.dump(nyse_prices,open('nyse.p','wb'))
pickle.dump(nyse_prices_two,open('nyse_two.p','wb'))
pickle.dump(nasdaq_prices,open('nasdaq.p','wb'))
pickle.dump(nasdaq_prices_two,open('nasdaq_two.p','wb'))




