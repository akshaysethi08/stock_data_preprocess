import pickle 
import numpy as np 
import os 
import sklearn 
from sklearn.ensemble import IsolationForest
from sklearn.neighbors import LocalOutlierFactor
from sklearn.svm import OneClassSVM

nd = pickle.load(open('nasdaq.p','rb'))
ny = pickle.load(open('nyse.p','rb'))

prices = nd['XELA'][0]
price_val = (prices.values) # could locate the date with back indexing  

'''
price_val = price_val.reshape(-1,1)


# alg1 
iso = IsolationForest()
out_iso = iso.fit_predict(price_val)
print (out_iso.shape)
indexes_iso = (out_iso == -1)
print (indexes_iso)
out1 = prices[indexes_iso]
print (out1)

# alg2 
osvm = OneClassSVM()
out_osvm = osvm.fit_predict(price_val)
print (out_osvm.shape)
indexes_osvm = ((out_osvm == -1))
print (indexes_osvm)
out2 = prices[indexes_osvm]
print (out2)

# alg3 
lof = LocalOutlierFactor()
out_lof = lof.fit_predict(price_val)
print (out_lof.shape)
indexes_lof = ((out_lof == -1))
print (indexes_lof)
out3 = prices[indexes_lof]
print (out3)
'''


#check for first
year = (np.mean(prices[:200]))
if (prices[0] > 10*year or prices[0] < year/10):
	prices[0] = year

for i in range(1,prices.shape[0]):

	if (prices[i] > 10*prices[i-1] or prices[i] < prices[i-1]/10):
		print ("flagged")
		print (prices[i])
		prices[i] = prices[i-1]





